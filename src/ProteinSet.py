"""
Copyright (C) 2018  Zahid Islam (dev@zahidislm.com)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

from mxnet import nd
from mxnet.gluon.data import Dataset
import pathlib as pl
import pandas as pd
from PIL import Image


class ProteinSet(Dataset):
    """Human Protein Atlas Dataset

    Attributes:
        dataframe (Dataframe): Dataframe containing Image IDS and any given labels.
    """

    def __init__(self, root_dir, is_train=True, csv_file=None):
        """
        Args:
            root_dir (str): Path to directory containing dataset images.
            is_train (bool, optional): If dataset belongs to a training set.
                Defaults to True.
            csv_file (str, optional): Full path for csv file containing Image IDs.
                Creates csv if no path is given.
        """

        self.root_dir = pl.Path(root_dir)
        self.is_train = is_train

        if csv_file:
            self.dataframe = pd.read_csv(csv_file)
        else:
            image_ids = set(file.name.split("_")[0]
                            for file in root_dir.iterdir())

            csv_data = {"Id": list(image_ids)}
            self.dataframe = pd.DataFrame(csv_data, columns=csv_data.keys())

    def __len__(self):
        return len(self.dataframe)

    def __getitem__(self, idx: int):
        """
        Args:
            idx (int): Sample index value.

        Returns:
            protein_image (NDArray): The protein of interest. (Has a green filter)
            stacked_filter_images (NDArray): Stacked landmark filters.
                Blue -> Nucleus
                Red -> Microtubules
                Yellow -> Endoplasmic Reticulum
            labels (List<int>): Protein's organelle localization labels if given.
        """

        image_id = self.dataframe.iloc[idx, 0]
        filter_colors = ["blue", "red", "yellow"]
        image_filter_paths = [self.root_dir /
                              f"{image_id}_{color}.png" for color in filter_colors]

        protein_image_path = self.root_dir / f"{image_id}_green.png"
        filter_images = [nd.array(Image.open(path)).astype(
            "float") for path in image_filter_paths]

        protein_image = nd.array(Image.open(
            protein_image_path)).astype("float")

        stacked_filter_images = nd.stack(*filter_images)

        if self.is_train:
            labels = self.dataframe.iloc[idx, 1].split(" ")

            return protein_image, stacked_filter_images, labels

        return protein_image, stacked_filter_images
