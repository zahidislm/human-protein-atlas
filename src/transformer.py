"""
Copyright (C) 2018  Zahid Islam (dev@zahidislm.com)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

from mxnet.gluon.data import Dataset
from mxnet.gluon.data.vision import transforms


class Transformer:
    """Runs transformation pipelines for dataset

    Attributes:
        transforms_list (List<str>): List of transforms selected by user.
            Defaults to None.
    """

    def __init__(self, data: Dataset, shape=None, transformer_args=None):
        """
        Args:
            data (Dataset): Selected dataset used for transformations
            shape (int or tuple of (W, H), optional): Shape of reshaped dataset if given.
                Defaults to None.
            transformer_args (Dict, optional): Dictionary of transformer functions as keys and
                named arguments as values.
                Defaults to None.
        """
        self.data = data
        self.shape = shape
        self.transformer_args = transformer_args
        self.transforms_list = transformer_args.keys() if transformer_args else None

    def run(self):
        """Runs the transformer

        Returns:
            transformed_data (Dataset): Dataset with all the transformations applied.
        """

        pipeline = [transforms.ToTensor()]

        if self.shape:
            pipeline.insert(0, transforms.Resize(self.shape))

        if self.transforms_list:
            # Finds the specifc transform function
            pipeline += [
                getattr(transforms, method)(self.transformer_args[method])
                if hasattr(transforms, method)
                else print(f"{method} not available.")
                for method in self.transforms_list
            ]

        pipeline.append(transforms.Normalize(0, 1))
        composed_transformer = transforms.Compose(pipeline)
        transformed_data = self.data.transform(composed_transformer)

        return transformed_data
