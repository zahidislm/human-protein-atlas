![alt text][logo]

[logo]: https://www.proteinatlas.org/images_static/logo_text.png "The Human Protein Atlas"

 Model capable of classifying mixed patterns of proteins in microscope images. Useful to identify a protein's location(s) & classification from an image. <br />

 The model trains on filtered images of a protein based on landmarks such as the nucleus, microtubules and the endoplasmic reticulum to classify all the organelle localization labels for each sample. There are 28 class labels in total... <br />

> 0. Nucleoplasm
> 1. Nuclear membrane
> 2. Nucleoli
> 3. Nucleoli fibrillar center
> 4. Nuclear speckles
> 5. Nuclear bodies
> 6. Endoplasmic reticulum
> 7. Golgi apparatus
> 8. Peroxisomes
> 9. Endosomes
> 10. Lysosomes
> 11. Intermediate filaments
> 12. Actin filaments
> 13. Focal adhesion sites
> 14. Microtubules
> 15. Microtubule ends
> 16. Cytokinetic bridge
> 17. Mitotic spindle
> 18. Microtubule organizing center
> 19. Centrosome
> 20. Lipid droplets
> 21. Plasma membrane
> 22. Cell junctions
> 23. Mitochondria
> 24. Aggresome
> 25. Cytosol
> 26. Cytoplasmic bodies
> 27. Rods & rings

### Requirements
* Python >= **3.6.7**
* Pipenv (run **pipenv install**) <br />

Download the data @ [Kaggle Competition](https://www.kaggle.com/c/human-protein-atlas-image-classification/)